import { Component } from "react";
import "./App.css";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Dashboard from "./component/Dashboard/Dashboard";
import Login from './component/login/Login';
import Register from './component/Register/Register';
import Cart from './component/Cart/Cart';
import SearchResult from "./component/SearchResult/SearchResult";
import ProductDetail from "./component/ProductItem/ProductDetail";
class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" exact component={Dashboard} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/cart" component={Cart} />
          <Route path="/searchResult/:id" children={<SearchResult />} />
          <Route path="/productPage/:id" children={<ProductDetail />} />
        </Switch>
      </Router>
    );
  }
}

export default App;
