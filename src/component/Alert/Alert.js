import React, { Component } from 'react'
import AlertIcon from './exclamation-triangle.svg'
import './css/alert.css';

class Alert extends Component{
    constructor(props) {
        super(props);
        this.state ={
            message: this.props.message
        }
    }

    render() {
        return (
            <div className="alert-container">
                <img src={AlertIcon} alt="" />
                <div id="alert-details">
                    <p>There was a problem</p>
                    {this.state.message}
                </div>
            </div>
        )
    }
}

export default Alert
