import React, { Component } from "react";
import "./css/login.css";
import AmazonLogo from "../image/amazon-logo-dark.png";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import LoginFooter from "../footer/LoginFooter";
import axios from "axios";
import Alert from "../Alert/Alert";
import { Link, withRouter } from "react-router-dom";

class Login extends Component {
  constructor(props) {
    document.title = "Login";
    super(props);
    this.state = {
      email: "",
      password: "",
      messsage: "",
    };
  }

  onChangeEmail = (event) => {
    this.setState({
      email: event.target.value,
    });
  };
  onChangePassword = (event) => {
    this.setState({
      password: event.target.value,
    });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    const {history} = this.props;
    try {
      const response = await axios.post(
        "https://vivek-amazon-clone.herokuapp.com/api/login",
        {
          email: this.state.email,
          password: this.state.password,
        }
      );
      localStorage.setItem("token", response.data.userDetails.token);
      history.push('/');
    } catch (error) {
      this.setState({ message: error.response.data.message });
    }
  };

  render() {
    const {
      handleSubmit, onChangeEmail, onChangePassword,
      state: { message },
    } = this;

    return (
      <div className="login-container">
        <Link to="/">
          <img id="amazon-logo-img" src={AmazonLogo} alt="" />
        </Link>
        {message !== undefined ? <Alert message={message} /> : ""}
        <Form id="login-form" onSubmit={handleSubmit}>
          <span id="title-sign">Sign-In</span>
          <FormGroup>
            <Label id="email-label">Email</Label>
            <Input
              type="email"
              name="email"
              id="email-input"
              onChange={onChangeEmail}
              required
            />
          </FormGroup>
          <FormGroup>
            <Label id="email-label">Password</Label>
            <Input
              type="password"
              name="password"
              id="email-input"
              onChange={onChangePassword}
              required
            />
          </FormGroup>

          <Button id="submit-btn" type="submit">
            Continue
          </Button>
          <div id="title-terms">
            By continuing, you agree to Amazon's Condition of Use and Privacy
            Notice.
          </div>
        </Form>
        <div id="title-new-amazon">
          <hr />
          New to Amazon?
          <hr />
        </div>
        <Form>
          <Button id="create-account-btn">
            <Link id="register-link" to="/register">
              Create your Amazon account
            </Link>
          </Button>
        </Form>
        <LoginFooter />
      </div>
    );
  }
}

export default withRouter(Login);
