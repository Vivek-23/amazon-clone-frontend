import axios from "axios";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
} from "reactstrap";
import AmazonFullfilled from "../image/amazon-fullfilled.png";
import ProductImage from "../image/item1.jpg";
import "./css/cartList.css";

class CartList extends Component {
  constructor(props) {
    console.log(props.product[0])
    super(props);
    this.state = {
      dropdownOpen: false,
      dropdownselect: 1,
    };
  }

  handleDeleteCart = async (event) => {
    event.preventDefault();
    console.log(this.props.product[0]);
    try {
      const config = {
        headers: { Authorization: localStorage.getItem("token") },
      };
      await axios.delete(
        `https://vivek-amazon-clone.herokuapp.com/api/deleteFromCart/${this.props.product[0]}`,
        config
      );
      window.location.reload();
    } catch (error) {
      console.log(error);
    }
  };

  changeQuantity = (event) => {
    this.setState({
      dropdownselect: event.currentTarget.textContent,
    });
    console.log(this.state.dropdownselect);
    this.props.parentCallback(this.state.dropdownselect);
    event.preventDefault();
  };

  toggle = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  };

  render() {
    const {
      state: { dropdownOpen, dropdownselect },
      handleDeleteCart,
      toggle,
      changeQuantity,
    } = this;

    return (
      <>
        <div id="item-list">
          <div id="item-image">
            <img src={ProductImage} alt="" />
          </div>
          <div id="item-details">
            <div id="item-name">{this.props.product[1]}</div>
            <span id="title-stock">In stock</span>
            <br />
            <span id="title-eligible">Eligible for free shipping</span>
            <br />
            <img src={AmazonFullfilled} alt="" />
            <div id="item-options">
              <Dropdown isOpen={dropdownOpen} toggle={toggle}>
                <DropdownToggle id="quantity-menu" caret>
                  Qty : {dropdownselect}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={changeQuantity}>1</DropdownItem>
                  <DropdownItem onClick={changeQuantity}>2</DropdownItem>
                  <DropdownItem onClick={changeQuantity}>3</DropdownItem>
                  <DropdownItem onClick={changeQuantity}>4</DropdownItem>
                  <DropdownItem onClick={changeQuantity}>5</DropdownItem>
                  <DropdownItem onClick={changeQuantity}>6</DropdownItem>
                  <DropdownItem onClick={changeQuantity}>7</DropdownItem>
                  <DropdownItem onClick={changeQuantity}>8</DropdownItem>
                  <DropdownItem onClick={changeQuantity}>9</DropdownItem>
                </DropdownMenu>
              </Dropdown>

              <div id="delete">
                <Link to="/cart" onClick={handleDeleteCart}>
                  delete
                </Link>
              </div>
            </div>
          </div>
          <div id="price-details">
            <h3>
              <span id="rupee-symbol">&#8377;</span>
              {this.props.product[2] * dropdownselect}.00
            </h3>
          </div>
        </div>
      </>
    );
  }
}

export default CartList;
