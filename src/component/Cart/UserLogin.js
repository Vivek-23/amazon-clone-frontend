import { Component } from "react";
import CartEmpty from "./CartEmpty";
import CartList from "./CartList";
import CartTotalPrice from "./CartTotalPrice";
import "./css/userLogin.css";

class UserLogin extends Component {

  constructor(props) {
    console.log(props.name)
    super(props);
    this.state = {
      total: ''
    }
  }

  handleCallback = (totalPrice) => {
    this.setState({
      total: totalPrice
    });
    console.log(this.state.total)
  }

  render() {
    let count = 0,
      totalPrice = 0;

    const { name } = this.props;
    return (
      <div className="cart-container">
        {name.length === 0 ? (
          <CartEmpty />
        ) : (
          <>
            <div id="classList">
              <h1>Shopping Cart</h1>
              {name.map((item) => {
                count++;
                totalPrice += parseInt(item.price);
                return (
                  <>
                    <CartList product={[item.id, item.productName, item.price]} parentCallback={this.handleCallback} />
                  </>
                );
              })}
            </div>
            
            <div id="cartTotalPrice">
              <CartTotalPrice price={[this.state.total * totalPrice, count]} />
            </div>
          </>
        )}
      </div>
    );
  }
}

export default UserLogin;
