import React, { Component } from "react";
import "./css/cartTotalPrice.css";
import { Button } from "reactstrap";
import AmazonProtectionLogo from "../image/Amazon-protection-logo.png";

class CartTotalPrice extends Component{
  render() {
    const {price} = this.props
    return (
      <div id="total-container">
        <img src={AmazonProtectionLogo} alt="" />
  
        <div id="subtotal-section">
          <div id="total-price">
            <i>Subtotal &#40;{price[1]} items&#41;: &#8377;</i>
            <span id="price">{price[0]}</span>
          </div>
          <Button id="buy-btn">Proceed to Buy</Button>
        </div>
      </div>
    );
  }
}

export default CartTotalPrice;
