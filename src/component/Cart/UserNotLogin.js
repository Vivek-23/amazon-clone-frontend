import React, { Component } from "react";
import "./css/useNotLogin.css";
import { Button } from "reactstrap";
import NotLoginImage from "../image/kettledesaturated.svg";
import { Link } from "react-router-dom";
class UserNotLogin extends Component{
  render() {
    return (
      <div className="container">
        <div id="img">
          <img src={NotLoginImage} alt="" />
        </div>
        <div id="details">
          <div id="title">Your Amazon Basket is empty</div>
          <div id="buttons">
          <Link to="/login"><Button id="signin-btn">Sign in to your account</Button></Link>
          <Link to="/register"><Button id="signup-btn">Sign up now</Button></Link>
          </div>
        </div>
      </div>
    );
  }
}

export default UserNotLogin;
