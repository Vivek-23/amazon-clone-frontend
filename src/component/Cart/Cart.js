import axios from "axios";
import React, { Component } from "react";
import MainFooter from "../footer/MainFooter";
import MainHeader from "../header/MainHeader";
import MainNavBar from "../navbar/MainNavBar";
import UserLogin from "./UserLogin";
import UserNotLogin from "./UserNotLogin";
import "./css/cart.css";
import Loader from "react-loader-spinner";
class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      itemList: [],
    };
  }

  fetchResponse = async (config) => {
    try {
      let response = await axios.get(
        "https://vivek-amazon-clone.herokuapp.com/api/cart",
        config
      );
      return response;
    } catch (err) {
      throw err;
    }
  };

  componentDidMount() {
    document.title = "Cart";
    const config = {
      headers: { Authorization: localStorage.getItem("token") },
    };
    this.fetchResponse(config)
      .then((res) => {
        this.setState({ itemList: res.data, error: false });
      })
      .catch((err) => {
        this.setState({ error: true });
      });
  }

  render() {
    const { error, itemList } = this.state;
    return (
      <div>
        <MainHeader />
        <MainNavBar />
        {error === null ? (
          <Loader
            className="loader"
            type="Bars"
            color="#131921"
            height={80}
            width={80}
          />
        ) : error ? (
          <UserNotLogin />
        ) : (
          <UserLogin name={itemList} />
        )}
        <MainFooter />
      </div>
    );
  }
}

export default Cart;
