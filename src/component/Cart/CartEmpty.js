import React, { Component } from 'react'
import './css/cartEmpty.css';
import NotLoginImage from "../image/kettledesaturated.svg";

class CartEmpty extends Component {
    render() {
        return (
            <div id="empty-container">
                <img id="empty-img" src={NotLoginImage} alt="" />
                <h1>Your Cart is Empty</h1>
            </div>
        )
    }
}

export default CartEmpty
