import React, { Component } from "react";
import MainFooter from "../footer/MainFooter";
import MainHeader from "../header/MainHeader";
import MainNavBar from "../navbar/MainNavBar";
import ProductImage from "../image/item1.jpg";
import "./css/searchResult.css";
import { Button } from "reactstrap";
import axios from "axios";
import { Link, withRouter } from "react-router-dom";
import Loader from "react-loader-spinner";

class SearchResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      description: [],
      price: "",
      error: false,
    };
  }

  getProductDetails = async () => {
    console.log("Id: " + this.props.match.params.id);
    try {
      const response = await axios(
        `https://vivek-amazon-clone.herokuapp.com/api/getSearchItem/${this.props.match.params.id}`
      );
      return response.data;
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.getProductDetails()
      .then((res) => {
        console.log(res);
        this.setState({
          name: res.name,
          description: res.description,
          price: res.price,
        });
      })
      .catch((err) => console.log(err));
  }

  render() {
    return (
      <div>
        <MainHeader />
        <MainNavBar />
        <em id="title-result">Search Results</em>
        {this.state.name === "" ? (
          <Loader type="Bars" color="#131921" height={80} width={80} />
        ) : (
          <div className="search-container">
            <div id="product-image">
              <img src={ProductImage} alt="" />
            </div>
            <div id="product-details">
              <div id="product-title">
                <Link
                  id="item-link"
                  to={{
                    pathname: `/productPage/${this.props.match.params.id}`,
                  }}
                >
                  {this.state.name}
                </Link>
              </div>
              <div id="price">
                <h4>{this.state.price}</h4>

                <span>Save 5,000 </span>
              </div>
              <span>FREE Delivery by Amazon</span>
              <br />
            </div>
          </div>
        )}
        <MainFooter />
      </div>
    );
  }
}

export default withRouter(SearchResult);
