import React, { Component } from "react";
import "./css/mainNavBar.css";
import { Button, List, ListInlineItem } from "reactstrap";
import { GiHamburgerMenu } from "react-icons/gi";
import { Link } from "react-router-dom";

class MainNavBar extends Component {
  render() {
    return (
      <div className="main-navbar">
        <List type="inline" id="list-items">
          <ListInlineItem id="item">
            <Link id="item-button" className="hamburger-menu">
              <GiHamburgerMenu id="burger-menu" />
              <span id="title-all">All</span>
            </Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Best Sellers</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Mobiles</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Prime</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Fashion</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Electronics</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">New Releases</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Customer Services</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Today's Deals</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Amazon Pay</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Home Kitchen</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Computers</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Toys Games</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Books</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Shell</Link>
          </ListInlineItem>
          <ListInlineItem id="item">
            <Link id="item-button">Shopping Made</Link>
          </ListInlineItem>
        </List>
      </div>
    );
  }
}

export default MainNavBar;
