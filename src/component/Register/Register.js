import React, { Component } from "react";
import "./css/register.css";
import AmazonLogo from "../image/amazon-logo-dark.png";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import LoginFooter from "../footer/LoginFooter";
import { Link } from "react-router-dom";
import axios from "axios";
import Alert from "../Alert/Alert";

class Register extends Component {
  constructor(props) {
    document.title = "Register";
    super(props);
    this.state = {
      name: "",
      phone: "",
      email: "",
      password: "",
      message: "",
    };
  }

  onChangeName = (event) => {
    this.setState({
      name: event.target.value,
    });
  };

  onChangePhone = (event) => {
    this.setState({
      phone: event.target.value,
    });
  };

  onChangeEmail = (event) => {
    this.setState({
      email: event.target.value,
    });
  };

  onChangePassword = (event) => {
    this.setState({
      password: event.target.value,
    });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post(
        "https://vivek-amazon-clone.herokuapp.com/api/register",
        {
          name: this.state.name,
          phoneNo: this.state.phone,
          email: this.state.email,
          password: this.state.password,
        }
      );
      window.location.href("/");
    } catch (error) {
      console.log(error)
      this.setState({ message: error.response });
    }
  };

  render() {
    const {
      state: { message },
      onChangeEmail,
      onChangeName,
      onChangePassword,
      onChangePhone,
      handleSubmit,
    } = this;

    return (
      <div className="register-container">
        <Link to="/">
          <img id="amazon-logo-img" src={AmazonLogo} alt="" />
        </Link>
        {message !== "" ? <Alert message={message} /> : ""}

        <Form id="register-form" onSubmit={handleSubmit}>
          <span id="title-sign">Create Account</span>
          <FormGroup>
            <Label id="name-label">Your Name</Label>
            <Input
              type="text"
              name="name"
              id="name-input"
              required
              onChange={onChangeName}
            />
          </FormGroup>
          <FormGroup>
            <Label id="mobile-label">Mobile number</Label>
            <Input
              type="text"
              name="mobile"
              id="mobile-input"
              required
              onChange={onChangePhone}
            />
          </FormGroup>
          <FormGroup>
            <Label id="email-label">Email</Label>
            <Input
              type="email"
              name="email"
              id="email-input"
              required
              onChange={onChangeEmail}
            />
          </FormGroup>
          <FormGroup>
            <Label id="password-label">Password</Label>
            <Input
              type="password"
              name="password"
              id="password-input"
              required
              onChange={onChangePassword}
            />
          </FormGroup>

          <Button id="submit-btn" type="submit">
            Continue
          </Button>
          <div id="title-terms">
            By continuing, you agree to Amazon's Condition of Use and Privacy
            Notice.
          </div>
          <div id="title-signin">
            <div>
              Already have an account? <Link to="/login">Sign in</Link>
            </div>
          </div>
        </Form>
        <LoginFooter />
      </div>
    );
  }
}
export default Register;
