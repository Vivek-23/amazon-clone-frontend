import axios from "axios";
import React, { Component } from "react";
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  InputGroup,
} from "reactstrap";

import "./css/headerSeachBar.css";
import InputSearchBar from "./InputSearchBar";

class HeaderSearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
      suggestion: "",
    };
  }

  toggle = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  };

  response = async () => {
    let res = await axios.get(
      "https://vivek-amazon-clone.herokuapp.com/api/searchitem"
    );
    return await res.data;
  };
  componentDidMount() {
    this.response()
      .then((data) => this.setState({ suggestion: data }))
      .catch((err) => console.log(err));
  }

  render() {
    const {
      toggle,
      state: { dropdownOpen, suggestion },
    } = this;
    return (
      <div>
        <div className="search-bar">
          <div id="search-bar-dropdown">
            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
              <DropdownToggle caret id="dropdown-main">
                All
              </DropdownToggle>
              <DropdownMenu id="dropdown-menu">
                <DropdownItem>All Categories</DropdownItem>
                <DropdownItem>Deals</DropdownItem>
                <DropdownItem>Alexa Skills</DropdownItem>
                <DropdownItem>Amazon Devices</DropdownItem>
                <DropdownItem>Amazon Fashion</DropdownItem>
                <DropdownItem>Amazon Pantry</DropdownItem>
                <DropdownItem>Appliances</DropdownItem>
                <DropdownItem>Apps &amp; Games</DropdownItem>
                <DropdownItem>Baby</DropdownItem>
                <DropdownItem>Beauty</DropdownItem>
                <DropdownItem>Books</DropdownItem>
                <DropdownItem>Car &amp; Motorbike</DropdownItem>
                <DropdownItem>Clothing &amp; Accessories</DropdownItem>
                <DropdownItem>Collectibles</DropdownItem>
                <DropdownItem>Computers &amp; Accessories</DropdownItem>
                <DropdownItem>Electronics</DropdownItem>
                <DropdownItem>Furniture</DropdownItem>
                <DropdownItem>Garden &amp; Outdoors</DropdownItem>
                <DropdownItem>Gift Cards</DropdownItem>
                <DropdownItem>Grocery &amp; Gourmet Foods</DropdownItem>
                <DropdownItem>Health &amp; Personal Care</DropdownItem>
                <DropdownItem>Home &amp; Kitchen</DropdownItem>
                <DropdownItem>Industrial &amp; Scientific</DropdownItem>
                <DropdownItem>Jewellery</DropdownItem>
                <DropdownItem>Kindle Store</DropdownItem>
                <DropdownItem>Luggage &amp; Bags</DropdownItem>
                <DropdownItem>Luxury Beauty</DropdownItem>
                <DropdownItem>Movies &amp; TV Shows</DropdownItem>
                <DropdownItem>Music</DropdownItem>
                <DropdownItem>Musical Instruments</DropdownItem>
                <DropdownItem>Office Products</DropdownItem>
                <DropdownItem>Pet Supplies</DropdownItem>
                <DropdownItem>Prime Video</DropdownItem>
                <DropdownItem>Shoes &amp; Handbags</DropdownItem>
                <DropdownItem>Software</DropdownItem>
                <DropdownItem>Sports, Fitness &amp; Outdoors</DropdownItem>
                <DropdownItem>Subscribe &amp; Save</DropdownItem>
                <DropdownItem>Tools &amp; Home Improvement</DropdownItem>
                <DropdownItem>Toys &amp; Games</DropdownItem>
                <DropdownItem>Under ₹500</DropdownItem>
                <DropdownItem>Video Games</DropdownItem>
                <DropdownItem>Watches</DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </div>
          <div id="search-bar-input">
            <InputGroup>
              <InputSearchBar suggestions={suggestion} />
            </InputGroup>
          </div>
        </div>
      </div>
    );
  }
}

export default HeaderSearchBar;
