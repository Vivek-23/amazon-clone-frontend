import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button, Dropdown, DropdownMenu, DropdownToggle } from "reactstrap";
import "./css/SignIn.css";

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
    };
  }

  toggle = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  };
  onMouseEnter = () => this.setState({ dropdownOpen: true });
  onMouseLeave = () => this.setState({ dropdownOpen: false });

  handleSignOut = () => {
    localStorage.clear();
    window.location.reload();
  };

  render() {
    const username = this.props.name;
    const name = this.props.name;

    const {
      state: { dropdownOpen },
      toggle,
      onMouseEnter,
      onMouseLeave,
      handleSignOut,
    } = this;

    return (
      <div>
        <Dropdown
          isOpen={dropdownOpen}
          toggle={toggle}
          onMouseOver={onMouseEnter}
          onMouseLeave={onMouseLeave}
        >
          <DropdownToggle caret id="dropdown-signin">
            <span id="login-hello">
              {name === "" ? <>Hello, Sign in</> : <>Hello, {username}</>}
            </span>
            <br />
            <span id="accountList">Account &#38; Lists</span>
          </DropdownToggle>
          <DropdownMenu id="dropmenu-signin">
            {name === "" ? (
              <div id="dropitem-signin">
                <Button id="dropdown-btn-signin">
                  <Link id="sign-link" to="/login">
                    Sign In
                  </Link>
                </Button>
                <span id="dropdown-register-link">
                  New Customer?{" "}
                  <Link id="register-link-drop" to="/register">
                    Start Here
                  </Link>
                </span>
              </div>
            ) : (
              ""
            )}
            <div id="dropitem-list">
              <div id="col-1">
                <ul>
                  <li id="title-list">Your Lists</li>
                  <li>
                    <Link id="link-list" to="#">
                      Vivek's Wish List
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Create a Wish List
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Find a Wish List
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Wish from Any Website
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Baby Wish List
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Discover Your Style
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Explore Showroom
                    </Link>
                  </li>
                </ul>
              </div>
              <div id="col-2">
                <ul>
                  <li id="title-list">Your Account</li>
                  <li>
                    <Link id="link-list" to="#">
                      Your Account
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Your Orders
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Your Wish List
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Your Recommendations
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Your Prime Membership
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Your Prime Video
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Your Subscribe & Save Items
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Memberships & Subscriptions
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Your Amazon Business Account
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Your Seller Account
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Manage Your Content and Devices
                    </Link>
                  </li>
                  <li>
                    <Link id="link-list" to="#">
                      Switch Accounts
                    </Link>
                  </li>
                  {name !== "" ? (
                    <li>
                      <Link id="link-list" to="/" onClick={handleSignOut}>
                        Sign Out
                      </Link>
                    </li>
                  ) : (
                    ""
                  )}
                </ul>
              </div>
            </div>
          </DropdownMenu>
        </Dropdown>
      </div>
    );
  }
}

export default SignIn;
