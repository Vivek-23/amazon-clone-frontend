import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Button, Input, InputGroupAddon } from "reactstrap";
import "./css/inputSearchBar.css";
import { BsSearch } from "react-icons/bs";
import axios from "axios";
import { Link } from "react-router-dom";

class InputSearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: "",
      productId: ""
    };
  }

  onChange = (e) => {
    const { suggestions } = this.props;
    const userInput = e.currentTarget.value;

    const filteredSuggestions = suggestions.filter(
      (suggestion) =>
        suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    this.setState({
      activeSuggestion: 0,
      filteredSuggestions,
      showSuggestions: true,
      userInput: e.currentTarget.value,
    });
  };

  onClick = async(e) => {

    e.preventDefault();

    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText,
    });
    
    try {
      let response = await axios.get(
        `https://vivek-amazon-clone.herokuapp.com/api/searchItemByName/${e.currentTarget.innerText}`,
        );
        console.log(response.data[0].id);
        this.setState({productId: response.data[0].id});
      } catch (err) {
        console.log(err);
      }
  };

  render() {
    const {
      onChange,
      onClick,
      state: {
        activeSuggestion,
        filteredSuggestions,
        showSuggestions,
        userInput,
      },
    } = this;

    let suggestionsListComponent;

    if (showSuggestions && userInput) {
      if (filteredSuggestions.length) {
        suggestionsListComponent = (
          <ul class="suggestions">
            {filteredSuggestions.map((suggestion, index) => {
              let className;

              if (index === activeSuggestion) {
                className = "suggestion-active";
              }

              return (
                <li className={className} key={suggestion} onClick={onClick}>
                  {suggestion}
                </li>
              );
            })}
          </ul>
        );
      } else {
        suggestionsListComponent = (
          <div className="no-suggestions">No results found</div>
        );
      }
    }
    return (
      <Fragment>
        <Input
          id="search-input"
          type="text"
          onChange={onChange}
          value={userInput}
          name={userInput}
        />
        {suggestionsListComponent}
        <InputGroupAddon addonType="append">
          <Button color="secondary" id="search-btn">
            <Link  to={{pathname: `/searchResult/${this.state.productId}`}}>
              <BsSearch color="black" id="search-icon" />
            </Link>
          </Button>
        </InputGroupAddon>
      </Fragment>
    );
  }
}

export default InputSearchBar;
