import axios from "axios";
import React, { Component } from "react";
import { Button } from "reactstrap";
import "./css/mainHeader.css";
import HeaderSearchBar from "./HeaderSeachBar";
import ShoppingCart from "./ShoppingCart";
import SignIn from "./SignIn";
import { withRouter } from "react-router-dom";

class MainHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      name: "",
      id: "",
    };
  }

  componentDidMount() {
    const config = {
      headers: { Authorization: localStorage.getItem("token") },
    };
    axios
      .get("https://vivek-amazon-clone.herokuapp.com/api/dashboard", config)
      .then((response) => {
        this.setState({
          name: response.data.name,
          email: response.data.email,
          id: response.data.id,
        });
      })
      .catch((error) => console.log(error));
  }

  homeLocate = (event) => {
    const { history } = this.props;
    event.preventDefault();
    history.push("/");
  };
  render() {
    const {
      homeLocate,
      state: { name },
    } = this;
    return (
      <div className="header">
        <div id="amazon-logo">
          <Button id="amazon-logo-btn" onClick={homeLocate}></Button>
        </div>
        <div id="header-address">
          <Button id="amazon-address-btn">
            <span id="hello-title">Hello</span>
            <span id="address-title">Select your address</span>
          </Button>
        </div>
        <div id="header-search-bar">
          <HeaderSearchBar />
        </div>
        <div id="header-login-section">
          <SignIn name={name} />
        </div>
        <div id="header-orders-section">
          <Button id="return-btn">
            <span id="return-title">Return</span>
            <span id="order-title">&#38; Orders</span>
          </Button>
        </div>
        <div id="header-cart-section">
          <ShoppingCart />
        </div>
      </div>
    );
  }
}

export default withRouter(MainHeader);
