import React, { Component } from "react";
import { Button } from "reactstrap";
import "./css/shoppingCart.css";
import ShoppingCartImg from "../image/PinClipart.com_las-vegas-strip-clip_3818015.png";
import { Link } from "react-router-dom";
import axios from "axios";
class ShoppingCart extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cartCount: 0
    }
  }
  
  fetchResponse = async (config) => {
    try {
      let response = await axios.get("https://vivek-amazon-clone.herokuapp.com/api/cart", config);
      return response;
    } catch (err) {
      throw err;
    }
  };

  componentDidMount(){
    const config = {
      headers: { Authorization: localStorage.getItem("token") },
    };
    this.fetchResponse(config)
      .then((res) => {
        this.setState({cartCount: res.data.length})
      })
      .catch((err) => {
        this.setState({error: true});
      });
  };

  render() {
    return (
      <div className="shopping-cart">
        <Button id="shopping-btn">
          <Link id="cart-link" to="/cart">
            <span id="item-number">{this.state.cartCount}</span>
            <img id="cart-img" src={ShoppingCartImg} alt="" />
            <span id="cart-title">Cart</span>
          </Link>
        </Button>
      </div>
    );
  }
}

export default ShoppingCart;
