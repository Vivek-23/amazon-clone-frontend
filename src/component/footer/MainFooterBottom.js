import React, { Component } from "react";
import "./css/mainFooterBottom.css";

class MainFooterBottom extends Component {
  render() {
    return (
      <div className="footer-bottom">
        <div id="bottom-list">
          <div id="list">
            <ul>
              <li id="bottom-list-title">AbeBooks</li>
              <li>Boors, art</li>
              <li>&#38; collection</li>
            </ul>
          </div>
          <div id="list">
            <ul>
              <li id="bottom-list-title">AbeBooks</li>
              <li>Boors, art</li>
              <li>&#38; collection</li>
            </ul>
          </div>
          <div id="list">
            <ul>
              <li id="bottom-list-title">AbeBooks</li>
              <li>Boors, art</li>
              <li>&#38; collection</li>
            </ul>
          </div>
          <div id="list">
            <ul>
              <li id="bottom-list-title">AbeBooks</li>
              <li>Boors, art</li>
              <li>&#38; collection</li>
            </ul>
          </div>
          <div id="list">
            <ul>
              <li id="bottom-list-title">AbeBooks</li>
              <li>Boors, art</li>
              <li>&#38; collection</li>
            </ul>
          </div>
          <div id="list">
            <ul>
              <li id="bottom-list-title">AbeBooks</li>
              <li>Boors, art</li>
              <li>&#38; collection</li>
            </ul>
          </div>
          <div id="list">
            <ul>
              <li id="bottom-list-title">AbeBooks</li>
              <li>Boors, art</li>
              <li>&#38; collection</li>
            </ul>
          </div>
          <div id="list">
            <ul>
              <li id="bottom-list-title">AbeBooks</li>
              <li>Boors, art</li>
              <li>&#38; collection</li>
            </ul>
          </div>
          <div id="list">
            <ul>
              <li id="bottom-list-title">AbeBooks</li>
              <li>Boors, art</li>
              <li>&#38; collection</li>
            </ul>
          </div>
        </div>
        <div id="bottom-content">
          <div id="content">Conditions of Use &#38; Sale</div>
          <div id="content">Privacy Notice</div>
          <div id="content">Interest-Based Ads</div>
          <div id="content">
            &#169; 1996-2021, Amazon.com, Inc. or its affiliates
          </div>
        </div>
      </div>
    );
  }
}

export default MainFooterBottom;
