import React, { Component } from 'react'
import AmazonLogo from '../image/amazon-png.png';
import './css/mainFooterMiddle.css';

class MainFooterMiddle extends Component {
    render() {
        return (
            <div className="footer-middle">
                <div id="logo-img" >
                    <img src={AmazonLogo} alt="" />
                </div>
                <div id="country-list">
                <span>Australia</span>
                <span>Brazil</span>
                <span>Canada</span>
                <span>China</span>
                <span>France</span>
                <span>Germany</span>
                <span>Italy</span>
                <span> Japan</span>
                <span>Mexico</span>
                <span>Netherlands</span>
                <span>Poland</span>
                <span>Singapore</span>
                <span>Spain</span>
                <span>Turkey</span>
                <span>United Arab Emirates</span>
                <span>United Kingdom</span>
                <span>United States</span>
                </div>
            </div>
        )
    }
}

export default MainFooterMiddle
