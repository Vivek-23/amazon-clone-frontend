import React, { Component } from "react";
import './css/loginFooter.css'
class LoginFooter extends Component{
  render() {
    return (
      <div className="footer">
        <div id="row">
          <div id="col-1">Condition of Use</div>
          <div id="col-1">Privacy Notice</div>
          <div id="col-1">Help</div>
        </div>
        <div id="title-copyright">&#169;1996-2021, Amazon.com, Inc. or its affiliates</div>
      </div>
    );
  }
}

export default LoginFooter;
