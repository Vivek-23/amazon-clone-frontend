import React, { Component } from "react";
import "./css/mainFooterTop.css";

class MainFooterTop extends Component {
  render() {
    return (
      <div className="footer-top">
        <div id="list-1">
          <ul>
            <li id="title-list">Get to Know Us</li>
            <li>About Us</li>
            <li>Careers</li>
            <li>Press Releases</li>
            <li>Amazon Careers</li>
            <li>Gift a Smile</li>
          </ul>
        </div>
        <div id="list-2">
          <ul>
            <li id="title-list">Connect with Us</li>
            <li>Facebook</li>
            <li>Twitter</li>
            <li>Instagram</li>
          </ul>
        </div>
        <div id="list-3">
          <ul>
            <li id="title-list">Make Money With Us</li>
            <li>Sell on Amazon</li>
            <li>Sell under Amazon Accelerator</li>
            <li>Amazon Global Setting</li>
            <li>Become an Affliate</li>
            <li>Fulfillment by Amazon</li>
            <li>Advertise Your Products</li>
            <li>Amazon Pay on Merchants</li>
          </ul>
        </div>
        <div id="list-4">
          <ul>
            <li id="title-list">Let Us Help You</li>
            <li>COVID-19 and Amazon</li>
            <li>Your Account</li>
            <li>Returns Centre</li>
            <li>100% Purchase Protection</li>
            <li>Amazon App Download</li>
            <li>Amazon Assistant Download</li>
            <li>Help</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default MainFooterTop;
