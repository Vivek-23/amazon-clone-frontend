import React, { Component } from "react";
import MainFooterBottom from "./MainFooterBottom";
import MainFooterMiddle from "./MainFooterMiddle";
import MainFooterTop from "./MainFooterTop";

class MainFooter extends Component {
  render() {
    return (
      <div>
        <MainFooterTop />
        <MainFooterMiddle />
        <MainFooterBottom />
      </div>
    );
  }
}

export default MainFooter;
