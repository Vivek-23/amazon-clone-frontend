import React, { Component} from "react";
import MainHeader from "../header/MainHeader";
import MainNavBar from "../navbar/MainNavBar";
import MainFooter from "../footer/MainFooter";
import axios from "axios";
import ItemCard from "../ProductItem/ItemCard";
import "./css/dashboard.css";
import Loader from "react-loader-spinner";

class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      list: []
    }
  } 

componentDidMount(){
    document.title = "Amazon | Dashboard";
    axios
      .get("https://vivek-amazon-clone.herokuapp.com/api/getAllItem")
      .then((res) => res.data)
      .then((data) => this.setState({list: data}))
      .catch((err) => console.log(err));
}

render() {
  const {list} = this.state
  console.log(list);
  return (
    <div>
      <MainHeader />
      <MainNavBar />
      <div id="card-section">
        {list.length === 0 ? (
          <Loader type="Bars" color="#131921" height={80} width={80} />
        ) : (
          Array.prototype.map.call(list, (item) => {
            return (
              <>
                <ItemCard name={[item.productName, item.price, item.id]} />
              </>
            );
          })
        )}
      </div>
      <MainFooter />
    </div>
  );

}
}

export default Dashboard;
