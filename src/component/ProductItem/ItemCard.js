import axios from "axios";
import React, { Component, useState } from "react";
import { Link, useHistory, withRouter } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardSubtitle,
  CardTitle,
} from "reactstrap";

import ProductImage from "../image/item1.jpg";
import "./css/itemCard.css";

class ItemCard extends Component {
  render() {
    const { name } = this.props;
    return (
      <div className="item-card">
        <Card id="card">
          <CardImg id="card-img" top src={ProductImage} alt="Card image cap" />
          <CardBody>
            <CardTitle tag="h5">
              <Link id="item-link" to={{ pathname: `/productPage/${name[2]}` }}>
                {name[0]}
              </Link>
            </CardTitle>
            <CardSubtitle tag="h6" className="mb-2 text-muted">
              &#8377;{name[1]}
            </CardSubtitle>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default withRouter(ItemCard);
