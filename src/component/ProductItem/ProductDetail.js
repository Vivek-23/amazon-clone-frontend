import axios from "axios";
import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { Button } from "reactstrap";
import MainFooter from "../footer/MainFooter";
import MainHeader from "../header/MainHeader";
import MainNavBar from "../navbar/MainNavBar";
import "./css/productDetail.css";
class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: [],
      price: "",
      error: false
    };
  }

  getProductDetails = async () => {
    try {
      const response = await axios(
        `https://vivek-amazon-clone.herokuapp.com/api/getSearchItem/${this.props.match.params.id}`
      );
      return response.data;
    } catch (error) {
      console.log(error);
    }
  };

  addCartItems = async (event) => {
    event.preventDefault();
    console.log(this.props.match.params.id)
    try {
      const config = {
        headers: { Authorization: localStorage.getItem("token") },
      };
      let response = await axios.get(
        `https://vivek-amazon-clone.herokuapp.com/api/addToCart/${this.props.match.params.id}`,
        config
      );
      window.location.reload();
    } catch (err) {
      this.setState({error: true});
    }
  };

  componentDidMount() {
    this.getProductDetails()
      .then((res) => {
        this.setState({
          name: res.name,
          description: res.description,
          price: res.price,
        });
      })
      .catch((err) => console.log(err));
  }

  render() {
    const { description, price, error,name } = this.state;
    const {history} = this.props;
    return (
      <div>
        <MainHeader />
        <MainNavBar />
        <div id="product-page">
          <img
            src="https://images-na.ssl-images-amazon.com/images/I/71sxlhYhKWL._SY741_.jpg"
            alt=""
          />
          <div id="product-detail">
            <span id="product-head">{name}</span>
            <span id="price-product">&#8377;{price}</span>
            <ul>
              {description.map((item) => {
                return (
                  <>
                    <li>{item}</li>
                  </>
                );
              })}
            </ul>
            <Button id="addcart-btn" onClick={this.addCartItems}>
              {error ? (
                history.push("/login")
              ) : (
                <Link id="cart-link" to="/cart">
                  Add to Cart
                </Link>
              )}
            </Button>
          </div>
        </div>
        <MainFooter />
      </div>
    );
  }
}

export default withRouter(ProductDetail);
